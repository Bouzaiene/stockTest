### Problem

You are given an array of prices where prices[i] is the price of a given stock on the ith day.
You work for a stock speculator. You want to maximize your employer's profit by helping her choose a single day to buy one stock and a different day in the future to sell it.
Write a function that returns the day on which to buy, the day on which to sell, and the maximum profit you can achieve from this transaction.

#### Instructions

- to test the function run

```
- npm install
- npm test

```
