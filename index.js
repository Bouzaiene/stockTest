const {
  prices6,
  prices4,
  prices3,
  prices5,
  prices7,
  prices,
  prices2,
} = require("./data");

const isTomorrowPriceCheaper = (todayPrice, tomorrowPrice) =>
  todayPrice >= tomorrowPrice;

const isValidStockData = (prices) => {
  if (!Array.isArray(prices)) {
    throw new Error("input data must be an array !");
  }
  if (prices.length < 2) {
    throw new Error("provided data is not valid !");
  }
};

const speculateStock = (prices) => {
  let buyDate;
  let sellDate;
  let maxProfit = 0;

  isValidStockData(prices);
  for (let i = 0; i <= prices.length - 1; i++) {
    for (let j = i + 1; j <= prices.length - 1; j++) {
      if (isTomorrowPriceCheaper(prices[i], prices[j])) {
        // if the next day price is lower then today price then we stop counting profit for today
        break;
      }
      let currentProfit = prices[j] - prices[i];

      if (currentProfit > maxProfit) {
        maxProfit = currentProfit;
        buyDate = i;
        sellDate = j;
      }
    }
  }
  if (maxProfit) {
    return { buyDate, sellDate, maxProfit };
  }
  return null;
};

const transaction = speculateStock(prices2);

console.log(transaction);

module.exports = { speculateStock };
