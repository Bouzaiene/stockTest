const assert = require("assert");
const { speculateStock } = require("../index");
const data = require("../data");

const res1 = { buyDate: 0, maxProfit: 9900, sellDate: 7 };
const res2 = { buyDate: 4, maxProfit: 799, sellDate: 6 };
const res3 = { buyDate: 8, maxProfit: 298, sellDate: 9 };
const res4 = { buyDate: 0, maxProfit: 100, sellDate: 4 };

describe("Test speculateStock", () => {
  it("should equal res1", () => {
    assert.deepStrictEqual(speculateStock(data.prices), res1);
  });
  it("should equal res2", () => {
    assert.deepStrictEqual(speculateStock(data.prices1), res2);
  });
  it("should equal res3", () => {
    assert.deepStrictEqual(speculateStock(data.prices2), res3);
  });
  it("should equal res4", () => {
    assert.deepStrictEqual(speculateStock(data.prices3), res4);
  });
  it("should return null", () => {
    assert.deepStrictEqual(speculateStock(data.prices4), null);
  });
  it("should throw error invalid data", () => {
    assert.throws(
      () => {
        speculateStock(data.prices5);
      },
      Error,
      "provided data is not valid !"
    );
  });
  it("should throw error invalid input data", () => {
    assert.throws(
      () => {
        speculateStock(data.prices6);
      },
      Error,
      "provided data is not valid !"
    );
  });
  it("should throw error invalid input data", () => {
    assert.throws(
      () => {
        speculateStock(data.prices6);
      },
      Error,
      "input data must be an array !"
    );
  });
});
